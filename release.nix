let
  sources = import ./nix/sources.nix {};
  pkgs    = import sources.nixpkgs   {};
  mcl                  = pkgs.callPackage sources.mcl                  {};
  mmseqs2              = pkgs.callPackage sources.mmseqs2              {};
  quick-multi-paranoid = pkgs.callPackage sources.quick-multi-paranoid {};
in

pkgs.callPackage ./default.nix { inherit mcl mmseqs2 quick-multi-paranoid; }
