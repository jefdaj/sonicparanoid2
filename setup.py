# -*- coding: utf-8 -*-
"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
from setuptools.extension import Extension
from setuptools.command.install import install
from distutils.version import LooseVersion
#from Cython.Build import cythonize
import sys
import os
import platform
import zipfile
from shutil import move, rmtree
# needed to compile quickparanoid during the installation
import subprocess


#from os import path
#here = path.abspath(path.dirname(__file__))

_CYTHON_INSTALLED = False
min_cython_ver = '0.29'
try:
    import Cython
    ver = Cython.__version__
    _CYTHON_INSTALLED = ver >= LooseVersion(min_cython_ver)
except ImportError:
    _CYTHON_INSTALLED = False
    raise ImportError('\nWARNING: SonicParanoid requires a version of Cython higher than {:s}:\npip install cython\n'.format(min_cython_ver))

# exit with an error if Cython is not installed
if not _CYTHON_INSTALLED:
    sys.stderr.write('\nWARNING: SonicParanoid requires a version of Cython equal or higher than {:s}:\npip install cython\n'.format(min_cython_ver))

# load cythonize if cython has been installed
if _CYTHON_INSTALLED:
    from Cython.Build import cythonize



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise



def untar(tarpath, outDir=os.getcwd(), debug=True):
    import tarfile
    if debug:
        print('Tar.gz path:\t%s'%tarpath)
        print('Output dir:\t%s'%outDir)
    #check if the tar file is valid
    if not tarfile.is_tarfile(tarpath):
        sys.stderr.write('\nERROR: %s is not a valid tar.gz file.'%tarpath)
        sys.exit(-2)
    #create the output directory if does not exist yet
    if outDir[-1] != '/':
        outDir += '/'
    #create output directory
    makedir(outDir)
    #change current directory
    cwd = os.getcwd()
    if os.path.dirname(outDir) != cwd:
        os.chdir(outDir)
    #open the tar file
    tar = tarfile.open(tarpath)
    tar.extractall()
    tar.close()
    #set the current directory to the starting one
    os.chdir(cwd)
    if debug:
        print('Extracted in %s'%outDir)



extensions = [
    Extension(
        "sonicparanoid.inpyranoid_c",
        ["sonicparanoid/inpyranoid_c.pyx"],
    ),
    Extension(
        "sonicparanoid.mmseqs_parser_c",
        ["sonicparanoid/mmseqs_parser_c.pyx"],
    ),
    Extension(
        "sonicparanoid.remap_tables_c",
        ["sonicparanoid/remap_tables_c.pyx"],
    ),
    Extension(
        "sonicparanoid.graph_c",
        ["sonicparanoid/graph_c.pyx"],
    ),
    Extension(
        "sonicparanoid.essentials_c",
        ["sonicparanoid/essentials_c.pyx"],
    ),
    Extension(
        "sonicparanoid.mcl_c",
        ["sonicparanoid/mcl_c.pyx"],
    ),
]

# To use a consistent encoding
from codecs import open
from os import path
from os import chdir

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README_PYPI.md'), encoding='utf-8') as f:
    long_description = f.read()

# constant variables to be used inside the setup function
LICENSE = 'GNU GENERAL PUBLIC LICENSE, Version 3.0 (GPLv3)'



# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    # There are some restrictions on what makes a valid project name
    # specification here:
    # https://packaging.python.org/specifications/core-metadata/#name
    name='sonicparanoid',  # Required
    # Versions should comply with PEP 440:
    # https://www.python.org/dev/peps/pep-0440/
    #
    # For a discussion on single-sourcing the version across setup.py and the
    # project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version = "1.3.4", # Required
    # This is a one-line description or tagline of what your project does. This
    # corresponds to the "Summary" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#summary
    description='SonicParanoid: fast, easy and accurate orthology inference',  # Required
    long_description=long_description,  # Optional
    long_description_content_type="text/markdown",
    url='http://iwasakilab.bs.s.u-tokyo.ac.jp/sonicparanoid/',  # Optional
    # This should be your name or the name of the organization which owns the project.
    author="Salvatore Cosentino",  # Optional
    # This should be a valid email address corresponding to the author listed above.
    author_email="salvo@gmail.com",  # Optional
    # license
    license=LICENSE,
    # Classifiers help users find your project by categorizing it.
    #
    # For a list of valid classifiers, see
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[  # Optional
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Science/Research',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8'
    ],
    # Note that this is a string of words separated by whitespace, not a list.
    keywords='bioinformatic inparanoid orthology_inference phylogeny evolution orthology',  # Optional
    #package_dir={'': 'sonicparanoid'},
    package_dir={'sonicparanoid': 'sonicparanoid/'},
    #packages = find_packages('sonicparanoid'),
    #packages = find_packages('sonicparanoid'),
    packages = ['sonicparanoid',],
    # required python version
    python_requires=">=3.6, <3.9",
    include_package_data=True,
    package_data={"sonicparanoid": ["example/test_output/*", "example/test_input/*", "bin/README.txt",
            "bin/guess_fast_pair.pckl",
            "README_PYPI.md",
            ]},  # Optional
    # required packages
    install_requires=["scipy>=1.2.1", "numpy>=1.18.0", "pandas>=1.0.0", "cython>=0.29.0", "setuptools>=24.2.0", "pip>=9.0.1", "biopython>=1.73", "mypy>=0.720", "psutil>=5.6.0", "scikit-learn>=0.22.0", "wheel>=0.32.0"], # specify minimum version

    # external to be compiled
    ext_modules = cythonize(extensions, compiler_directives={"language_level": 3}),
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    entry_points={  # Optional
        'console_scripts': [
            'sonicparanoid = sonicparanoid.sonic_paranoid:main',
            'sonicparanoid-get-test-data = sonicparanoid.get_test_data:main',
            'sonicparanoid-extract = sonicparanoid.sonic_paranoid_extract:main',
        ],
    },

    # List additional URLs that are relevant to your project as a dict.
    #
    # This field corresponds to the "Project-URL" metadata fields:
    # https://packaging.python.org/specifications/core-metadata/#project-url-multiple-use
    #
    # Examples listed include a pattern for specifying where the package tracks
    # issues, where the source is hosted, where to say thanks to the package
    # maintainers, and where to support the project financially. The key is
    # what's used to render the link text on PyPI.
    project_urls={  # Optional
        'Source': 'https://gitlab.com/salvo981/sonicparanoid2',
    },
)
