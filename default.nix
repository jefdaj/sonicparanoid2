{ python3, python3Packages, fetchurl, mmseqs2, mcl, quick-multi-paranoid }:

# let
#   myPython = python3.withPackages (ps: with ps; [
#     # from install_requires in setup.py
#     scipy
#     numpy
#     pandas
#     cython
#     setuptools
#     pip
#     biopython
#     mypy
#     psutil
#     sklearn-deap # TODO is this the right scikit-learn?
#     wheel

#     # needed to fix build errors
#     # setuptools
#   ]);

python3Packages.buildPythonPackage rec {
  pname = "sonicparanoid";
  version = "1.3.4-nix";

  src = ./.;
  # src = fetchurl {
  #   url = "https://files.pythonhosted.org/packages/90/ad/aadec7847f30cd1403e6a6f4c04caebdb3c3837a8641fa0befd59a9f8cbf/sonicparanoid-1.3.4.tar.gz";
  #   sha256 = "1wqzlc2zsl8yz1qi4xv2vqfa6bdpfwf2r04j496y1bbyw2g6v6ps";
  # };

  propagatedBuildInputs = with python3Packages; [
    # setuptools

    # from install_requires in setup.py
    scipy
    numpy
    pandas
    cython
    setuptools
    pip
    biopython
    mypy
    psutil
    sklearn-deap # TODO is this the right scikit-learn?
    wheel

    # TODO does this help?
    # mcl
    # quick-multi-paranoid
    # mmseqs2
  ];

  buildInputs = [
    # not sure if useful:
    # cd-hit http://weizhongli-lab.org/cd-hit/
    # muscle
    # biopython
    # hmmer
    mcl
    mmseqs2
    quick-multi-paranoid
    # mmseqs2
    # numpy
    # pandas
    # cython
    # sh
    # psutil
    # mypy
  ];

  # TODO get the tests working! probably just need packages here?
  doCheck = false;
  # checkInputs = with pypiPython.packages; [];

  patches = [
    # TODO which of these are still needed? update them
    # ./add-nix-dependency-paths.patch
    # ./disable-version-checks.patch
    # ./ignore-divide-by-zero.patch # TODO is this a bad idea?
  ];

  # Once the patch above has replaced the setup code with Nix vars,
  # this fills in the full paths. Overall pretty hacky, but seems to work.
  inherit mcl mmseqs2 quick-multi-paranoid;
  postPatch = ''
    for f in sonicparanoid/*.py*; do
      substituteAllInPlace "$f"
    done
  '';

  meta = {
    # TODO write this
  };
}
