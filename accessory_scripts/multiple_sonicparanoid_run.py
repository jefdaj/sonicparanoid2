# -*- coding: utf-8 -*-
"""Execute the SonicParanoid in both complete and essential modes."""
import os
import sys
import sh
import time
import shutil
import subprocess


########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # create the possible values for sensitivity value
    sensList = []
    for i in range(1, 9):
        sensList.append(float(i))
        for j in range(1, 10):
            fval = float(i + float(j / 10.))
            if fval <= 7.5:
                sensList.append(fval)
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description='Tandem SonicParanoid [essential and complete runs]',  usage='%(prog)s -i <INPUT_DIRECTORY> -o <OUTPUT_DIRECTORY>[options]', prog='sonicparanoid')
    # Mandatory arguments
    parser.add_argument('-i', '--input-directory', type=str, required=True, help='Directory containing the proteomes (in FASTA format) of the species to be analyzed.', default=None)
    parser.add_argument('-o', '--output-directory', type=str, required=True, help='The directory in which the results will be stored.', default=None)

    # General run options
    parser.add_argument('-p', '--prefix-name', type=str, required=False, help='Prefix that will be used for the run names.', default="sonic")
    parser.add_argument('-t', '--threads', type=int, required=False, help='Number of parallel 1-CPU jobs to be used. Default=4', default=4)
    parser.add_argument('-d', '--debug', required=False, help='Output debug information. (WARNING: extremely verbose)', default=False, action='store_true')
    parser.add_argument('-l', '--log-directory', type=str, required=True, help='Directory in which the log files are stored.')

    # pairwise orthology inference
    parser.add_argument('-noidx', '--no-indexing', required=False, help='Avoid the creation of indexes for MMseqs2 databases. IMPORTANT: while this saves starting storage space it makes MMseqs2 slightly slower.\nThe results might also be sligthy different.', default=False, action='store_true')
    parser.add_argument('-op', '--output-pairs', required=False, help='Output a text file with all the orthologous relations.', default=False, action='store_true')

    # Update runs
    parser.add_argument('-ot', '--overwrite-tables', required=False, help='This will force the re-computation of the ortholog tables. Only missing alignment files will be re-computed.', default=False, action='store_true')
    parser.add_argument('-ow', '--overwrite', required=False, help='Overwrite previous runs and execute it again. This can be useful to update a subset of the computed tables.', default=False, action='store_true')

    # run mode: split in two matrixes, or in a single one [sonic130b5]
    parser.add_argument('-1mtx', '--single-matrix', required=False, help='Perform essentials and complete alignments in sequence, using a single job-matrix.', default=False, action='store_true')

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



def makedir(path):
    """Create a directory including the intermediate directories in the path if not existing."""
    try:
        os.makedirs(path)
    except OSError:
        if not os.path.isdir(path):
            raise

########### MAIN ############

def main():

    #Get the parameters
    args, parser = get_params()
    debug = args.debug
    singleMtxMode = args.single_matrix
    inDir = None
    if args.input_directory is not None:
        inDir = "{:s}/".format(os.path.realpath(args.input_directory))
    # output dir
    outDir = '{:s}'.format(os.path.realpath(args.output_directory))
    # check that the input directory has been provided
    if (inDir is None):
        sys.stderr.write('\nERROR: no input species.\n')
        parser.print_help()

    # inizialize useful variables
    threads = args.threads
    runID = args.prefix_name
    logDir: str = os.path.realpath(args.log_directory)
    makedir(logDir)

    print('\nRun START:\t{:s}'.format(str(time.asctime(time.localtime(time.time())))))
    print('Multiple SonicParanoid runs will be executed in complete and essentials modes:')
    print('Run ID:\t{:s}'.format(runID))
    print('Input directory: {:s}'.format(inDir))
    print('Output directory: {:s}'.format(outDir))
    print('Threads:\t{:d}'.format(threads))

    # get the directory in which the scripts is stored
    srcDir = os.path.dirname(os.path.abspath(__file__))

    # Tandem script to be executed
    tandemScript: str = os.path.join(srcDir, "tandem_sonicparanoid_run.py")
    if singleMtxMode: # this is slower because the first cycle of alignments is performed 2 times
        tandemScript = os.path.join(srcDir, "tandem_sonicparanoid_run_single_mtx.py")

    # set the modes at which the run should be executed
    tandemTmpCmd: str = ""
    modes = []
    modes.append("fast")
    modes.append("default")
    modes.append("sensitive")
    modes.append("most-sensitive")

    # execute the tandem runs

    for mode in modes:
        tandemTmpCmd = "python3 {:s} -i {:s} -o {:s} -m {:s} -t {:d} -l {:s} -p {:s} -op".format(tandemScript, inDir, outDir, mode, threads, logDir, runID)
        print()
        print(tandemTmpCmd)
        print()
        #'''
        process = subprocess.Popen(tandemTmpCmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_val, stderr_val = process.communicate() #get stdout and stderr
        del stdout_val
        process.wait()
        print(stderr_val)





if __name__ == "__main__":
    main()


