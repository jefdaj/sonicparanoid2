"""Delete alignments stored in a list"""
import os
import sys
import shutil

########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # create the possible values for sensitivity value
    # define the parameter list
    import argparse
    usage = '\nProvide the directory with alignments and the file with the pairs\n'
    parser = argparse.ArgumentParser(description='Remove specific alignments',  usage='%(prog)s -i <DIRECTORY_WITH_ALIGNMENTS>[options] -p <FILE_WITH_PAIRS>', prog='remove_aln')
    #start adding the command line options
    parser.add_argument('-i', '--aln-directory', type=str, required=True, help='The directory in which the alignment files are stored.', default=None)
    parser.add_argument('-p', '--pairs-file', type=str, required=True, help='File with pairs.', default=None)
    parser.add_argument('-b', '--remove-both', required=False, help='Given a pair A-B, also remove B-A.', default=False, action='store_true')
    parser.add_argument('-d', '--debug', required=False, help='Output debug information.', default=False, action='store_true')
    args = parser.parse_args()
    return (args, parser)



########### MAIN ############

def main():

    #Get the parameters
    args, parser = get_params()
    #set the required variables
    debug = args.debug
    # alignments dir
    alnDir = os.path.realpath(args.aln_directory)
    # pairs file
    pairsPath = os.path.realpath(args.pairs_file)
    removeBoth = args.remove_both

    # will contain the pairs that have been removed
    pairsDict = {}

    # read the list of pairs
    with open(pairsPath, "rt") as fd:
        for ln in fd:
            pair = ln.rstrip("\n")
            a, b = pair.split("-", 1)

            if not pair in pairsDict:
                tmpPath = os.path.join(alnDir, pair)
                if os.path.isfile(tmpPath):
                    pairsDict[pair] = None
                    os.remove(tmpPath)
            # remove the other pair if requested
            if removeBoth:
                pair = "{:s}-{:s}".format(b, a)
                if not pair in pairsDict:
                    tmpPath = os.path.join(alnDir, pair)
                    if os.path.isfile(tmpPath):
                        pairsDict[pair] = None
                        os.remove(tmpPath)

    print("Removed alignment files:\t{:d}".format(len(pairsDict)))

if __name__ == "__main__":
    main()


