"""Split the relation IDs in pairwise relation file"""
import os
import sys
import shutil

########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # create the possible values for sensitivity value
    # define the parameter list
    import argparse
    usage = '\nProvide a file with pairwise ortholog relations\n'
    parser = argparse.ArgumentParser(description='Remove specific alignments',  usage='%(prog)s -i <PAIRWISE RELATION FILE>[options] -p <NEW_FILE_WITH_RELATIONS>', prog='split_qfo11')
    #start adding the command line options
    parser.add_argument('-i', '--input-pairs', type=str, required=True, help='File with relations.', default=None)
    parser.add_argument('-p', '--pairs-file', type=str, required=True, help='File with split pairwise relationships.', default=None)
    parser.add_argument('-d', '--debug', required=False, help='Output debug information.', default=False, action='store_true')
    args = parser.parse_args()
    return (args, parser)



########### MAIN ############

def main():

    #Get the parameters
    args, parser = get_params()
    #set the required variables
    debug = args.debug
    # alignments dir
    oldRel = os.path.realpath(args.input_pairs)
    # pairs file
    outPath = os.path.realpath(args.pairs_file)

    if debug:
        print("split_qfo11 :: START")
        print("Input relationships: {:s}".format(oldRel))
        print("Output relationships: {:s}".format(outPath))

    # open the output file
    ofd = open(outPath, "wt")
    wcnt: int = 0
    # read the list of pairs
    with open(oldRel, "rt") as fd:
        for ln in fd:
            pair = ln.rstrip("\n")
            a, b = pair.split("\t", 1)
            ofd.write("{:s}\t{:s}\n".format(a.split("_", 1)[1], b.split("_", 1)[1]))
            wcnt += 1

    print("Wrote relations:\t{:d}".format(wcnt))

if __name__ == "__main__":
    main()
