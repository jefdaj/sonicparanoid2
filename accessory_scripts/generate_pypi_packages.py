"""
 Create PyPI packages for OSX and LINUX (source).
"""

import os
import sys
from shutil import which
from subprocess import run
import platform



##### MAIN #####
def main() -> None:

    isDarwin = True
    # check OS
    OS = platform.system()
    if not OS == 'Darwin':
        isDarwin = False
        sys.stderr.write("WARNING: you must use a MacOS system to generate the packages bdist binary package.")
        sys.stderr.write("\nThe MacOS bdist package will not be created.\n")

    # check python installation
    pythonPath = which("python3")
    if pythonPath is None:
        sys.stderr.write("ERROR: the command \"python3\" was not found. Please install python3.")
        sys.exit(-5)
    # set log paths
    # get path to the script
    pySrcDir = os.path.dirname(os.path.abspath(__file__))
    setupPath = os.path.join(os.path.dirname(pySrcDir), "setup.py")
    # check the extans of the setup file
    if not  os.path.isfile(setupPath):
        sys.stderr.write(f"ERROR: the setup file \n{setupPath}\n was not found in the directory\n{pySrcDir}\n")
        sys.exit(-2)
    # generate the sdist source packages
    stdoutPath:str = os.path.join(pySrcDir, "generate_sdist.stdout.txt")
    stderrPath:str = os.path.join(pySrcDir, "generate_sdist.stderr.txt")
    # create the log files
    fdout = open(stdoutPath, "w")
    fderr = open(stderrPath, "w")
    bldCmd: str = f"{pythonPath} {setupPath} sdist"
    # use run
    run(bldCmd, shell=True, stdout=fdout, stderr=fderr)
    # close the log files
    fdout.close()
    fderr.close()

    # generate the bdist (OSX) package
    if isDarwin:
        stdoutPath = os.path.join(pySrcDir, "generate_bdist.stdout.txt")
        stderrPath = os.path.join(pySrcDir, "generate_bdist.stderr.txt")
        # python3(setupPath, "bdist_wheel", _out=stdoutPath, _err=stderrPath)
        # create the log files
        fdout = open(stdoutPath, "w")
        fderr = open(stderrPath, "w")
        bldCmd = f"{pythonPath} {setupPath} bdist_wheel"
        # use run
        run(bldCmd, shell=True, stdout=fdout, stderr=fderr)
        # close the log files
        fdout.close()
        fderr.close()

    # print some info on how to upload the packages
    print("PyPI packages generated successfully, in order to upload to PyPI run the following command:")
    print("twine upload --sign </dist/distribution_file_name>")
    print("\nor to upload a test version, execute the following:")
    print("twine upload --repository-url https://test.pypi.org/legacy/ --sign </dist/distribution_file_name>")

    print("\nTo install a package from the test PyPI repository use the following command:")
    print("pip3 install --index-url https://test.pypi.org/simple/ <package_name>")

if __name__ == "__main__":
    main()
