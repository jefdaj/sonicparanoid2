# -*- coding: utf-8 -*-
"""Execute the SonicParanoid in both complete and essential modes."""
import os
import sys
import sh
import time
import shutil


########### FUNCTIONS ############
def get_params():
    """Parse and analyse command line parameters."""
    # create the possible values for sensitivity value
    sensList = []
    for i in range(1, 9):
        sensList.append(float(i))
        for j in range(1, 10):
            fval = float(i + float(j / 10.))
            if fval <= 7.5:
                sensList.append(fval)
    # define the parameter list
    import argparse
    parser = argparse.ArgumentParser(description='Tandem SonicParanoid [essential and complete runs]',  usage='%(prog)s -i <INPUT_DIRECTORY> -o <OUTPUT_DIRECTORY>[options]', prog='sonicparanoid')
    # Mandatory arguments
    parser.add_argument('-i', '--input-directory', type=str, required=True, help='Directory containing the proteomes (in FASTA format) of the species to be analyzed.', default=None)
    parser.add_argument('-o', '--output-directory', type=str, required=True, help='The directory in which the results will be stored.', default=None)

    # General run options
    parser.add_argument('-p', '--prefix-name', type=str, required=False, help='Prefix that will be used for the run names.', default="sonic")
    parser.add_argument('-sh', '--shared-directory', type=str, required=False, help='The directory in which the alignment files are stored. If not specified it will be created inside the main output directory.', default=None)
    parser.add_argument('-t', '--threads', type=int, required=False, help='Number of parallel 1-CPU jobs to be used. Default=4', default=4)
    parser.add_argument('-at', '--force-all-threads', required=False, help='Force using all the requested threads.', action="store_true")
    parser.add_argument('-sm', '--skip-multi-species', required=False, help='Skip the creation of multi-species ortholog groups.', default=False, action='store_true')
    parser.add_argument('-d', '--debug', required=False, help='Output debug information. (WARNING: extremely verbose)', default=False, action='store_true')
    parser.add_argument('-l', '--log-directory', type=str, required=True, help='Directory in which the log files are stored.')

    # pairwise orthology inference
    parser.add_argument('-m', '--mode', required=False, help='SonicParanoid execution mode. The default mode is suitable for most studies. Use sensitive or most-sensitive if the input proteomes are not closely related.', choices=['fast', 'default', 'sensitive', 'most-sensitive'], default='default')
    parser.add_argument('-se', '--sensitivity', type=float, required=False, help='Sensitivity for MMseqs2. This will overwrite the --mode option.', default=None)
    parser.add_argument('-ml', '--max-len-diff', type=float, required=False, help='Maximum allowed length-difference-ratio between main orthologs and canditate inparalogs.\nExample: 0.5 means one of the two sequences could be two times longer than the other\n 0 means no length difference allowed; 1 means any length difference allowed. Default=0.5', default=0.5)
    parser.add_argument('-db', '--mmseqs-dbs', type=str, required=False, help='The directory in which the database files for MMseqs2 will be stored. If not specified it will be created inside the main output directory.', default=None)
    parser.add_argument('-noidx', '--no-indexing', required=False, help='Avoid the creation of indexes for MMseqs2 databases. IMPORTANT: while this saves starting storage space it makes MMseqs2 slightly slower.\nThe results might also be sligthy different.', default=False, action='store_true')
    parser.add_argument('-op', '--output-pairs', required=False, help='Output a text file with all the orthologous relations.', default=False, action='store_true')
    parser.add_argument('-qfo11', '--qfo-2011', required=False, help='Output a text file with all the orthologous relations formatted to be uploaded to the QfO benchmark service.\nNOTE: implies --output-pairs', default=False, action='store_true')
    parser.add_argument('-ka', '--keep-raw-alignments', required=False, help='Do not delete raw MMseqs2 alignment files. NOTE: this will triple the space required for storing the results.', default=False, action='store_true')

    # Ortholog groups inference
    parser.add_argument('-I', '--inflation', type=float, required=False, help='Affects the granularity of ortholog groups. This value should be between 1.2 (very coarse) and 5 (fine grained clustering). Default=1.5', default=1.5)
    parser.add_argument('-slc', '--single-linkage', required=False, help='Use single-linkage-clustering (MultiParanoid-like). NOTE: by default MCL clustering is used.', default=False, action='store_true')
    parser.add_argument('-mgs', '--max-gene-per-sp', type=int, required=False, help='Limits the maximum number of genes per species in the multi-species output table. This option reduces the verbosity of the multi-species output and only affects single-linkage-clustering. Default=30', default=30)

    # Update runs
    parser.add_argument('-ot', '--overwrite-tables', required=False, help='This will force the re-computation of the ortholog tables. Only missing alignment files will be re-computed.', default=False, action='store_true')
    parser.add_argument('-ow', '--overwrite', required=False, help='Overwrite previous runs and execute it again. This can be useful to update a subset of the computed tables.', default=False, action='store_true')
    #parser.add_argument('-u', '--update', type=str, required=False, help='Update the ortholog tables database by adding or removing input proteomes. Performs only required alignments (if any) for new species pairs, and re-compute the ortholog groups.\nNOTE: an ID for the update must be provided.', default=None)
    parser.add_argument('-rs', '--remove-old-species', required=False, help='Remove alignments and pairwise ortholog tables related to species used in a previous run. This option should be used when updating a run in which some input proteomes were modified or removed.', default=False, action='store_true')
    parser.add_argument('-un', '--update-input-names', required=False, help='Remove alignments and pairwise ortholog tables for an input proteome used in a previous which file name conflicts with a newly added species. This option should be used when updating a run in which some input proteomes or their file names were modified.', default=False, action='store_true')

    # parse the arguments
    args = parser.parse_args()

    return (args, parser)



########### MAIN ############

def main():

    #Get the parameters
    args, parser = get_params()
    debug = args.debug
    inDir = None
    if args.input_directory is not None:
        inDir = "{:s}/".format(os.path.realpath(args.input_directory))
    # output dir
    outDir = '{:s}'.format(os.path.realpath(args.output_directory))
    # check that the input directory has been provided
    if (inDir is None):
        sys.stderr.write('\nERROR: no input species.\n')
        parser.print_help()

    # Pair-wise tables directory
    pairwiseDbDir = os.path.join(outDir, "orthologs_db/")
    # Runs directory
    runsDir = os.path.join(outDir, "runs/")
    # set the update variable
    update_run = False
    # check that the snapshot file exists
    snapshotFile = os.path.join(outDir, "snapshot.tsv")
    if update_run:
        # check that it is not the first run
        if not os.path.isfile(snapshotFile):
            sys.stderr.write("\nWARNING: no snapshot file found. The run will considered as the first one.\n")
            update_run = False
    else:
        # force the variable to true if a snapshot exists
        if os.path.isfile(snapshotFile):
            update_run = True

    # Optional directories setup
    alignDir = None
    if args.shared_directory is not None:
        alignDir = "{:s}/".format(os.path.realpath(args.shared_directory))
    else:
        alignDir = os.path.join(outDir, 'alignments/')
    dbDirectory = None
    if args.mmseqs_dbs is not None:
        dbDirectory = "{:s}/".format(os.path.realpath(args.mmseqs_dbs))
    else:
        dbDirectory = os.path.join(outDir, "mmseqs2_databases/")
    threads = args.threads
    #coff = args.cutoff
    coff = 40
    owOrthoTbls = args.overwrite_tables
    skipMulti = args.skip_multi_species
    runMode = args.mode
    slc: bool = args.single_linkage
    maxGenePerSp = args.max_gene_per_sp
    # set the sensitivity value for MMseqs2
    sensitivity = 4.0 # default
    if runMode == 'sensitive':
        sensitivity = 6.0
    elif runMode == 'fast':
        sensitivity = 2.5
    elif runMode == 'most-sensitive':
        sensitivity = 7.5
    overwrite = args.overwrite
    if overwrite:
        owOrthoTbls = True
        # remove all the mmseqs2 index files
        if os.path.isdir(dbDirectory):
            for f in os.listdir(dbDirectory):
                fpath = os.path.join(dbDirectory, f)
                try:
                    if os.path.isfile(fpath):
                        os.unlink(fpath)
                    elif os.path.isdir(fpath): rmtree(fpath)
                except Exception as e:
                    print(e)

    # set sensitivity using a user spcified value if needed
    if args.sensitivity:
        if 1. <= args.sensitivity <= 7.5:
            sensitivity = round(args.sensitivity, 1)
            print('WARNING: the run mode \'%s\' will be overwritten by the custom MMseqs sensitivity value of %s.\n'%(runMode, str(args.sensitivity)))
        else:
            sys.stderr.write('\nERROR: the sensitivity parameter must have a value between 1.0 and 7.5\n')
            sys.exit(-6)

    # set the maximum length difference allowed if difference from default
    if args.max_len_diff != 0.5:
        if not (0. <= args.max_len_diff <= 1.0):
            sys.stderr.write('\nERROR: the length difference ratio must have a value between 0 and 1.\n')
            sys.exit(-6)
    # set the variable to control the creation of orthologous pairs
    output_relations = args.output_pairs
    if args.qfo_2011:
        output_relations = True
    # set the variable for MMseqs2 database indexing
    idx_dbs = True
    if args.no_indexing:
        idx_dbs = False

    # Prepare the run name
    runID = args.prefix_name
    # the run id should have the following format

    runDir = os.path.join(runsDir, runID)
    # Pair-wise tables directory
    tblDir = os.path.join(runDir, "pairwise_orthologs/")
    # Directory with ortholog groups
    multiOutDir = os.path.join(runDir, 'ortholog_groups/')

    # set the MCL inflation rate
    inflation: float = args.inflation
    if (inflation < 1.2) or (inflation > 5):
        sys.stderr.write("\nWARNING: The inflation rate parameter must be between 1.2 and 5.0 while it was set to {:.2f}\n".format(inflation))
        sys.stderr.write("It will be automatically set to 1.5\n")
        inflation = 1.5

    print('\nRun START:\t{:s}'.format(str(time.asctime(time.localtime(time.time())))))
    print('SonicParanoid will be executed in complete and essentials modes:')
    print('Run ID:\t{:s}'.format(runID))
    print('Run directory: {:s}'.format(runDir))
    print('Input directory: {:s}'.format(inDir))
    print('Output directory: {:s}'.format(outDir))
    print('Alignments directory: {:s}'.format(alignDir))
    print('Pairwise tables directory: {:s}'.format(tblDir))
    print('Directory with ortholog groups: {:s}'.format(multiOutDir))
    print('Runs directory: {:s}'.format(runsDir))
    print('Update run:\t{:s}'.format(str(update_run)))
    print('Create pre-filter indexes:\t{:s}'.format(str(idx_dbs)))
    print('Complete overwrite:\t{:s}'.format(str(overwrite)))
    print('Re-create ortholog tables:\t{:s}'.format(str(owOrthoTbls)))
    print('Threads:\t{:d}'.format(threads))
    print('Run mode:\t%s (MMseqs2 s=%s)'%(runMode, str(sensitivity)))
    if not args.single_linkage:
        print('MCL inflation:\t{:.2f}'.format(inflation))

    # set the project id
    if not args.sensitivity is None:
        runMode = "custom{:s}".format("".join(str(sensitivity).split(".", 1)))
    runID = "{:s}_{:s}_{:d}cpus_ml{:s}".format(runID, runMode, threads, "".join(str(args.max_len_diff).split(".", 1)) )
    # add other extra info in the naming
    if not idx_dbs:
        runID = "{:s}_noidx".format(runID)
    if overwrite:
        runID = "{:s}_ow".format(runID)
    if owOrthoTbls:
        if not overwrite:
            runID = "{:s}_ot".format(runID)
    if output_relations:
        runID = "{:s}_op".format(runID)
    if skipMulti:
        runID = "{:s}_sm".format(runID)
    if debug:
        runID = "{:s}_d".format(runID)

    # now prepare the two runs
    from sh import sonicparanoid
    #sonicparanoid("--help")

    logDir: str = os.path.realpath(args.log_directory)

    # perform the run with complete alignments
    tmpRunName: str = "{:s}_complete".format(runID)

    # remove old snapshot file
    snapPath: str = os.path.join(outDir, "snapshot.tsv")

    if os.path.isfile(snapPath):
        print("\nRemoving old snapshot file:\n{:s}".format(snapPath))
        os.remove(snapPath)

    #logPath: str = os.path.join(logDir, "{:s}_log.txt".format(tmpRunName))
    logPath: str = os.path.join(logDir, "{:s}_Tandem_log.txt".format(runID))
    print("\nThe log file for the normal run will be stored in:\n{:s}".format(logPath))

    # open the output file
    ofd = open(logPath, "wt")

    #sonicparanoid(i=inDir, o=outDir, t=threads, m=args.mode, se=args.sensitivity, complete_aln=True, skip_multi_species=True, output_pairs=True, p=tmpRunName, _out=logPath)

    # Overwrite everything at the real first start
    sonicparanoid(i=inDir, o=outDir, t=threads, m=args.mode, complete_aln=True, skip_multi_species=True, output_pairs=True, p=tmpRunName, overwrite=True, _out=ofd)

    # remove the alignments
    alnFilePathCa1: str = os.path.join(alignDir, "aln_ex_times_ca1_{:s}.tsv".format(tmpRunName))
    alnFilePathCa2: str = os.path.join(alignDir, "aln_ex_times_ca2_{:s}.tsv".format(tmpRunName))

    rmCnt = rdCnt = 0
    with open(alnFilePathCa2, "r") as ifd:
        for ln in ifd:
            rdCnt += 1
            tmpPath = os.path.join(alignDir, ln.split("\t", 1)[0])
            if os.path.isfile(tmpPath):
                os.remove(tmpPath)
                rmCnt += 1

    print("Read alignment files:\t{:d}".format(rdCnt))
    print("Removed alignment files:\t{:d}".format(rmCnt))

    # copy the alignment files to the log directory
    shutil.copy(alnFilePathCa1, logDir)
    shutil.copy(alnFilePathCa2, logDir)

    # compress the file with relations
    from sh import pigz
    # set the path to the relations file
    relationsPath: str = os.path.join(outDir, "runs/{:s}/ortholog_relations/ortholog_pairs.{:s}.tsv".format(tmpRunName, tmpRunName))
    # compress and copy the relations file
    pigz("-9", "-p", "4", relationsPath)
    shutil.copy("{:s}.gz".format(relationsPath), logDir)
    #sys.exit("DEBUG")

    # perform the essential alignments
    tmpRunName: str = "{:s}_essentitals".format(runID)
    #logPath: str = os.path.join(logDir, "{:s}_log.txt".format(tmpRunName))
    print("\nThe log file for the essential run will be stored in:\n{:s}".format(logPath))

    #sonicparanoid(i=inDir, o=outDir, t=threads, m=args.mode, se=args.sensitivity, ca=False, skip_multi_species=True, output_pairs=True, p=tmpRunName, overwrite_tables=True, _out=logPath)
    sonicparanoid(i=inDir, o=outDir, t=threads, m=args.mode, skip_multi_species=True, output_pairs=True, p=tmpRunName, overwrite_tables=True, _out=ofd)

    # copy the alignment files to the log directory
    alnFilePathRa: str = os.path.join(alignDir, "aln_ex_times_ra_{:s}.tsv".format(tmpRunName))
    shutil.copy(alnFilePathRa, logDir)

    # set the path to the relations file
    relationsPath: str = os.path.join(outDir, "runs/{:s}/ortholog_relations/ortholog_pairs.{:s}.tsv".format(tmpRunName, tmpRunName))
    # compress and copy the relations file
    pigz("-9", "-p", "4", relationsPath)
    shutil.copy("{:s}.gz".format(relationsPath), logDir)

    ofd.close()


if __name__ == "__main__":
    main()


